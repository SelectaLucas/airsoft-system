const electron = require('electron')
const path = require('path')
const url = require('url')

process.env.NODE_ENV = 'development'

const { app, BrowserWindow, Menu, ipcMain } = electron;

let mainWindow
let lancamentoWindow

app.on('ready', function () {
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 720,
        show: false,
        backgroundColor: '#1d2127',
        maximizable: false,
        resizable: false,
    })

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'dashboard.html'),
        protocol: 'file:',
        slashes: true
    }))

    mainWindow.once('ready-to-show', _ => mainWindow.show())

    mainWindow.on('closed', _ => app.quit())

    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)

    Menu.setApplicationMenu(mainMenu)
})

function createAddWindow() {
    lancamentoWindow = new BrowserWindow({
        width: 300,
        height: 200,
        resizable: false,
        show: false,
        backgroundColor: '#1d2127',
        maximizable: false,
        parent: mainWindow,
        fullscreen: true
    })

    lancamentoWindow.loadURL(url.format({
        pathname: path.join(__dirname, './src/areas/games/lancamento_missel/pages/lancamento.html'),
        protocol: 'file:',
        slashes: true,
    }))

    lancamentoWindow.once('ready-to-show', _ => lancamentoWindow.show())

    lancamentoWindow.on('close', _ => lancamentoWindow = null)
}

ipcMain.on('item:add', (e, item) => {
    mainWindow.webContents.send('item:add', item)
    lancamentoWindow.close()
})

const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Add Item',
                click() {
                    createAddWindow();
                }
            },
            {
                label: 'Clear Items',
                click() {
                    mainWindow.webContents.send('item:clear');
                }
            },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    }
];

if (process.platform == 'darwin')
    mainMenuTemplate.unshift({})

if (process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                role: 'reload'
            },
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools()
                }
            }
        ]
    })
}
else
    mainWindow.removeMenu()
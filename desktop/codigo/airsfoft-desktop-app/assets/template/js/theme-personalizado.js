window.theme = {};

// Theme Common Functions
window.theme.fn = {

    getOptions: function (opts) {

        if (typeof (opts) == 'object') {

            return opts;

        } else if (typeof (opts) == 'string') {

            try {
                return JSON.parse(opts.replace(/'/g, '"').replace(';', ''));
            } catch (e) {
                return {};
            }

        } else {

            return {};

        }

    }

};


// Data Tables - Config
(function ($) {

    'use strict';

    // we overwrite initialize of all datatables here
    // because we want to use select2, give search input a bootstrap look
    // keep in mind if you overwrite this fnInitComplete somewhere,
    // you should run the code inside this function to keep functionality.
    //
    // there's no better way to do this at this time :(
    if ($.isFunction($.fn['dataTable'])) {

        $.extend(true, $.fn.dataTable.defaults, {
            oLanguage: {
                sLengthMenu: '_MENU_ Registros por p&aacutegina',
                sProcessing: '<i class="fa fa-spinner fa-spin"></i> Carregando',
                sSearch: ''
            },
            fnInitComplete: function (settings, json) {
                // select 2
                if ($.isFunction($.fn['select2'])) {
                    $('.dataTables_length select', settings.nTableWrapper).select2({
                        theme: 'bootstrap',
                        minimumResultsForSearch: -1
                    });
                }

                var options = $('table', settings.nTableWrapper).data('plugin-options') || {};

                // search
                var $search = $('.dataTables_filter input', settings.nTableWrapper);

                $search
                    .attr({
                        placeholder: typeof options.searchPlaceholder !== 'undefined' ? options.searchPlaceholder : 'Procurar...'
                    })
                    .removeClass('form-control-sm').addClass('form-control pull-right');

                if ($.isFunction($.fn.placeholder)) {
                    $search.placeholder();
                }
            }
        });

    }

}).apply(this, [jQuery]);

// Scroll to Top
(function (theme, $) {

    theme = theme || {};

    $.extend(theme, {

        PluginScrollToTop: {

            defaults: {
                wrapper: $('body'),
                offset: 150,
                buttonClass: 'scroll-to-top',
                iconClass: 'fa fa-chevron-up',
                delay: 500,
                visibleMobile: false,
                label: false
            },

            initialize: function (opts) {
                initialized = true;

                this
                    .setOptions(opts)
                    .build()
                    .events();

                return this;
            },

            setOptions: function (opts) {
                this.options = $.extend(true, {}, this.defaults, opts);

                return this;
            },

            build: function () {
                var self = this,
                    $el;

                // Base HTML Markup
                $el = $('<a />')
                    .addClass(self.options.buttonClass)
                    .attr({
                        'href': '#',
                    })
                    .append(
                    $('<i />')
                        .addClass(self.options.iconClass)
                    );

                // Visible Mobile
                if (!self.options.visibleMobile) {
                    $el.addClass('hidden-mobile');
                }

                // Label
                if (self.options.label) {
                    $el.append(
                        $('<span />').html(self.options.label)
                    );
                }

                this.options.wrapper.append($el);

                this.$el = $el;

                return this;
            },

            events: function () {
                var self = this,
                    _isScrolling = false;

                // Click Element Action
                self.$el.on('click', function (e) {
                    e.preventDefault();
                    $('body, html').animate({
                        scrollTop: 0
                    }, self.options.delay);
                    return false;
                });

                // Show/Hide Button on Window Scroll event.
                $(window).scroll(function () {

                    if (!_isScrolling) {

                        _isScrolling = true;

                        if ($(window).scrollTop() > self.options.offset) {

                            self.$el.stop(true, true).addClass('visible');
                            _isScrolling = false;

                        } else {

                            self.$el.stop(true, true).removeClass('visible');
                            _isScrolling = false;

                        }

                    }

                });

                return this;
            }

        }

    });

}).apply(this, [window.theme, jQuery]);

// Scrollable
(function (theme, $) {

    theme = theme || {};

    var instanceName = '__scrollable';

    var PluginScrollable = function ($el, opts) {
        return this.initialize($el, opts);
    };

    PluginScrollable.updateModals = function () {
        PluginScrollable.updateBootstrapModal();
    };

    PluginScrollable.updateBootstrapModal = function () {
        var updateBoostrapModal;

        updateBoostrapModal = typeof $.fn.modal !== 'undefined';
        updateBoostrapModal = updateBoostrapModal && typeof $.fn.modal.Constructor !== 'undefined';
        updateBoostrapModal = updateBoostrapModal && typeof $.fn.modal.Constructor.prototype !== 'undefined';
        updateBoostrapModal = updateBoostrapModal && typeof $.fn.modal.Constructor.prototype.enforceFocus !== 'undefined';

        if (!updateBoostrapModal) {
            return false;
        }

        var originalFocus = $.fn.modal.Constructor.prototype.enforceFocus;
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            originalFocus.apply(this);

            var $scrollable = this.$element.find('.scrollable');
            if ($scrollable) {
                if ($.isFunction($.fn['themePluginScrollable'])) {
                    $scrollable.themePluginScrollable();
                }

                if ($.isFunction($.fn['nanoScroller'])) {
                    $scrollable.nanoScroller();
                }
            }
        };
    };

    PluginScrollable.defaults = {
        contentClass: 'scrollable-content',
        paneClass: 'scrollable-pane',
        sliderClass: 'scrollable-slider',
        alwaysVisible: true,
        preventPageScrolling: true
    };

    PluginScrollable.prototype = {
        initialize: function ($el, opts) {
            if ($el.data(instanceName)) {
                return this;
            }

            this.$el = $el;

            this
                .setData()
                .setOptions(opts)
                .build();

            return this;
        },

        setData: function () {
            this.$el.data(instanceName, this);

            return this;
        },

        setOptions: function (opts) {
            this.options = $.extend(true, {}, PluginScrollable.defaults, opts, {
                wrapper: this.$el
            });

            return this;
        },

        build: function () {
            this.options.wrapper.nanoScroller(this.options);

            return this;
        }
    };

    // expose to scope
    $.extend(theme, {
        PluginScrollable: PluginScrollable
    });

    // jquery plugin
    $.fn.themePluginScrollable = function (opts) {
        return this.each(function () {
            var $this = $(this);

            if ($this.data(instanceName)) {
                return $this.data(instanceName);
            } else {
                return new PluginScrollable($this, opts);
            }

        });
    };

    $(function () {
        PluginScrollable.updateModals();
    });

}).apply(this, [window.theme, jQuery]);
